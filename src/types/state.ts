interface State {
  modal: {
    id: string,
    open: boolean,
    text: string,
    action: string
  },
  posts: Array<Post>,
  newPost: Post,
  updatePost: Post,
  updateMode: {
    discardChanges: boolean,
    revertCanceled: boolean,
    anyChanges: boolean,
    lastPostVersion : Post
  },
  alerts: {
    [key: string]: Array<AlertData>
  }
}

export interface AlertData {
  text: string,
  id: string
}


export interface Post {
  id: string,
  title: string,
  tasks: Array<Task>
}

export interface Task{
  id: string,
  text: string,
  checked: boolean
  mode: string
}

export default State
