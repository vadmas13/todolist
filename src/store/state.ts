import stateTypes from './../types/state'

export default <stateTypes>{
  updateMode:{
    discardChanges: false,
    revertCanceled: false,
    anyChanges: false,
    lastPostVersion: {
      id:  '',
      title: '',
      tasks: []
    }
  },
  modal: {
    open: false,
    id: '',
    text: '',
    action: ''
  },
  newPost: {
    id:  '',
    title: '',
    tasks: []
  },
  updatePost: {
    id:  '',
    title: '',
    tasks: []
  },
  posts: [
    {
      id: '1',
      title: 'Hello World',
      tasks: [
        {
          id: '1',
          text: 'How many time i would be programmer in real life',
          checked: false,
          mode: 'read'
        },
        {
          id: '2',
          text: 'The simple task to beginers',
          checked: true,
          mode: 'read'
        },
        {
          id: '3',
          text: 'The simple task to middle',
          checked: false,
          mode: 'read'
        }
      ]
    }
  ],
  alerts: {
    errors: [

    ],
    success: [

    ]
  }
}
