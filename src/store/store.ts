import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from "vuex-persistedstate";

import newPostActions from "./actions/newPost";
import actionsModal from "./actions/actionsModal";
import actionsPosts from './actions/posts'
import actionsUpdatePost from './actions/actionsUpdatePost'
import actionsAlert from "./actions/actionsAlert";

import newPostMutations from "./mutations/newPost";
import mutationModal from "./mutations/mutationModal";
import mutationPosts from './mutations/mutationPosts'
import mutationUpdatePost from './mutations/mutationUpdatePost'
import mutationsAlert from "./mutations/mutationsAlert";

import getters from './getters/getters'
import state from './state'


Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    ...state
  },
  mutations: {
    ...newPostMutations,
    ...mutationModal,
    ...mutationPosts,
    ...mutationUpdatePost,
    ...mutationsAlert
  },
  actions: {
    ...newPostActions,
    ...actionsModal,
    ...actionsPosts,
    ...actionsUpdatePost,
    ...actionsAlert
  },
  getters: {
    ...getters
  },
  plugins: [
    createPersistedState({
      paths: ['posts']
    })
  ]
});
