import { Commit } from "vuex";

export default {
  DELETE_POST({commit}: {commit: Commit}){
    commit('DELETE_POST_MUTATION');
    commit('ADD_ALERT_MUTATION', { text: 'Post was deleted', mode: 'success'})
  }
}
