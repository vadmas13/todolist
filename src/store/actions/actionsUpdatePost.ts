import { Commit  } from 'vuex';


export default{
  SET_UPDATE_STATE({ commit }: { commit: Commit }, id: string){
    commit('SET_UPDATE_STATE_MUTATION', id)
    commit('SET_INITIAL_UPDATE_MODE_MUTATION', id)
  },
  HANDLE_UPDATE_TITLE( { commit }: { commit: Commit }, value: string){
    commit('UPDATE_TITLE_VALUE', value);
    commit('SET_CHANGES_UPDATE_MODE');
  },
  ADD_TASK_UPDATE({ commit }: { commit: Commit }, value: string){
    commit('ADD_TASK_UPDATE_MUTATION', value);
    commit('SET_CHANGES_UPDATE_MODE');
  },
  DISCARD_CHANGES_APPLY({ commit }: { commit: Commit }){
    commit('DISCARD_CHANGES_APPLY_MUTATION');
  },
  REVERT_CHANGES_APPLY({ commit }: { commit: Commit }){
    commit('REVERT_CHANGES_APPLY_MUTATION');
  },
  CHANGE_CHECKED_STATE({ commit }: { commit: Commit }, id: string){
    commit('CHANGE_CHECKED_STATE_MUTATION', id);
    commit('SET_CHANGES_UPDATE_MODE');
  }
}
