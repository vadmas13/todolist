import {Commit} from "vuex";

export default {
  CLOSE_MODAL({commit}: {commit: Commit}){
    commit('CLOSE_MODAL_MUTATION')
  },
  OPEN_MODAL({commit}: {commit: Commit}, data: {id: string, text: string, action?:string}){
    commit('OPEN_MODAL_MUTATIONS', data)
  }
}
