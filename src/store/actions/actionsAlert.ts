import {Commit} from "vuex";

export default {
  ADD_ERROR({commit}: {commit: Commit}, data: {text: string}){
    commit('ADD_ALERT_MUTATION', { text: data.text, mode: 'errors'})
  },
  ADD_SUCCESS({commit}: {commit: Commit}, data: {text: string}){
    commit('ADD_ALERT_MUTATION', { text: data.text, mode: 'success'})
  }
}
