import { Commit  } from 'vuex';


export default {
  HANDLE_CHANGE_TITLE( { commit }: { commit: Commit }, value: string){
    commit('SET_TITLE_VALUE', value);
  },
  ADD_TASK( { commit }: { commit: Commit }, text: string){
    commit('ADD_TASK_MUTATION', text);
    commit('SET_CHANGES_UPDATE_MODE');
  },
  ADD_POST( { commit }: {commit: Commit }, id? : string){
    commit('ADD_POST_MUTATION', id);
    if(!id){
      commit('CLEAR_NEW_POST_MUTATION');
    }else{
      commit('SET_INITIAL_UPDATE_MODE_MUTATION', id);
      commit('SET_UPDATE_STATE_MUTATION', id)
    }
  },
  REMOVE_TASK_NEW_POST( { commit }: {commit: Commit }, taskData: {id: string, updateMode? : boolean}){
    commit('REMOVE_TASK_NEW_POST_MUTATION', taskData);
    commit('SET_CHANGES_UPDATE_MODE');
  },
  UPDATE_MODE_NEW_POST({ commit }: { commit: Commit }, taskData: {id: string, updateMode? : boolean}){
    commit('UPDATE_MODE_NEW_POST_MUTATION', taskData);
  },
  UPDATE_TASK_NEW_POST({ commit }: {commit: Commit }, taskData: {id: string, text: string, updateMode?: boolean} ){
    commit('UPDATE_TASK_NEW_POST_MUTATION', taskData );
    commit('SET_CHANGES_UPDATE_MODE');
  },
  CANCEL_MODE_NEW_POST({ commit }: {commit: Commit }, taskData: {id: string, updateMode?: boolean} ){
    commit('CANCEL_TASK_NEW_POST_MUTATION', taskData )
  }
}
