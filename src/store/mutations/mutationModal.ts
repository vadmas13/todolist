import stateTypes from './../../types/state'

export default {
  CLOSE_MODAL_MUTATION: (state: stateTypes) => {
    state.modal.open = false;
    state.modal.id = ''
  },
  OPEN_MODAL_MUTATIONS: (state: stateTypes, data: {id: string, text: string, action?: string}) => {
    state.modal.open = true;
    state.modal.id = data.id;
    state.modal.text = data.text;
    if(data.action){
      state.modal.action = data.action;
    }
  }

}
