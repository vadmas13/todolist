import stateTypes from './../../types/state'
import {uuid} from "uuidv4";

export default {
  ADD_ALERT_MUTATION: (state: stateTypes, data: { text: string, mode: string }) => {
    const id: string = uuid();
    state.alerts[data.mode] = [{ text: data.text, id },...state.alerts[data.mode]];
    setTimeout(() => {
      state.alerts[data.mode] = [...state.alerts[data.mode].filter(item => item.id !== id)];
    }, 3000)
  }
}
