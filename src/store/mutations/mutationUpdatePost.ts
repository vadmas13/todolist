import stateTypes from './../../types/state'
import { Post } from './../../types/state'
import {uuid} from "uuidv4";

export default {
  SET_UPDATE_STATE_MUTATION:(state: stateTypes, id: string) => {
    const statePost: Post = state.posts.find(post => post.id === id)!;
    state.updatePost = {...statePost};
    state.updateMode.lastPostVersion = {...statePost};
  },
  UPDATE_TITLE_VALUE: (state: stateTypes, value: string) => {
    state.updatePost.title = value;
  },
  ADD_TASK_UPDATE_MUTATION: (state: stateTypes, text: string) => {
    state.updatePost.tasks = [...state.updatePost.tasks, { id: uuid(), text: text, checked: false, mode: 'read' } ];
  },
  DISCARD_CHANGES_APPLY_MUTATION: (state: stateTypes) => {
    state.updateMode.discardChanges = false;
    state.updateMode.revertCanceled = true;

    state.posts.find((post, i) => {
      if(post.id === state.updatePost.id){
        const revertCanceled = state.updateMode.lastPostVersion !;

        state.updateMode.lastPostVersion = {...state.updatePost, tasks: [...state.updatePost.tasks]};
        state.updatePost = {...revertCanceled, tasks: [...revertCanceled.tasks]};
        return true;
      }
    })
  },
  REVERT_CHANGES_APPLY_MUTATION: (state: stateTypes) => {
    state.updateMode.discardChanges = true;
    state.updateMode.revertCanceled = false;

    state.posts = [...state.posts.map((post, i) => {
      if(post.id === state.updatePost.id){
        const revertCanceled: Post = state.updateMode.lastPostVersion !;

        state.updateMode.lastPostVersion = post;
        state.updatePost = revertCanceled;
      }
      return post
    })]
  },
  SET_CHANGES_UPDATE_MODE: (state: stateTypes) => {
    state.updateMode.discardChanges = true;
    state.updateMode.anyChanges = true;
  },
  SET_INITIAL_UPDATE_MODE_MUTATION: (state: stateTypes) => {
    state.updateMode.discardChanges = false;
    state.updateMode.revertCanceled = false;
    state.updateMode.anyChanges = false;
  },
  CHANGE_CHECKED_STATE_MUTATION: (state: stateTypes, id: string) => {
    state.updatePost.tasks = [...state.updatePost.tasks.map(task => {
      if(task.id === id){
        return {
          ...task, checked: !task.checked
        }
      }else{
        return task
      }
    })]
  }
}
