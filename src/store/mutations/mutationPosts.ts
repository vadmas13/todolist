import stateTypes from './../../types/state'

export default {
  DELETE_POST_MUTATION: (state: stateTypes) => {
    if(state.modal.id){
      state.posts = [...state.posts.filter(post => post.id !== state.modal.id)]
    }
  },

}
