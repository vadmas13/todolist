import { uuid } from 'uuidv4';
import stateTypes from './../../types/state'
import { Post } from './../../types/state'


export default {
  SET_TITLE_VALUE: (state: stateTypes, value: string) => {
    state.newPost.title = value;
  },
  ADD_TASK_MUTATION: (state: stateTypes, text: string) => {
    state.newPost.tasks = [ { id: uuid(), text: text, checked: false, mode: 'read' },...state.newPost.tasks ];
  },
  ADD_POST_MUTATION: (state: stateTypes, id? : string) => {

    if(id && state.updateMode.anyChanges){
      state.posts = [...state.posts.map(post => {
        if(post.id === id){
          const ewPostState = state.updatePost;
          state.updateMode.lastPostVersion = ewPostState;
          return ewPostState
        }else{
          return post
        }
      })]

    }else{
      state.newPost.id = uuid();
      state.posts = [state.newPost,...state.posts]
    }

  },
  CLEAR_NEW_POST_MUTATION: (state: stateTypes) => {
    state.newPost = { id: uuid(), title: '', tasks: [] }
  },
  REMOVE_TASK_NEW_POST_MUTATION: (state: stateTypes, taskData: {id: string, updateMode? : boolean}) => {
    let base: Post;
    if(taskData.updateMode){
      base = state.updatePost;
    }else{
      base = state.newPost;
    }
    base = {...base, tasks: [...base.tasks.filter(task => task.id !== taskData.id)]}

    if(taskData.updateMode){
      state.updatePost = base;
    }else{
      state.newPost = base;
    }
  },
  UPDATE_MODE_NEW_POST_MUTATION: (state: stateTypes, taskData: {id: string, updateMode? : boolean})  => {
    let base: Post;
    if(taskData.updateMode){
      base = state.updatePost;
    }else{
      base = state.newPost;
    }
    base = {...base, tasks: [...base.tasks.map(task => {
        if(task.id === taskData.id){
          return{
            ...task, mode: 'update'
          }
        }else{
          return{
            ...task, mode: 'read'
          }
        }
      })]}

    if(taskData.updateMode){
      state.updatePost = base;
    }else{
      state.newPost = base;
    }
  },
  UPDATE_TASK_NEW_POST_MUTATION: (state: stateTypes, taskData: {id: string, text: string, updateMode?:boolean})  => {
    let base;
    if(taskData.updateMode){
      base = state.updatePost;
    }else{
      base = state.newPost;
    }
    base = {...base, tasks: [...base.tasks.map(task => {
        if(task.id === taskData.id){
          return {
            id: taskData.id,
            mode: 'read',
            text: taskData.text,
            checked: task.checked
          }
        }else{
          return task
        }
      })]}

    if(taskData.updateMode){
      state.updatePost = {...base, tasks: [...base.tasks]};
    }else{
      state.newPost = base;
    }

  },
  CANCEL_TASK_NEW_POST_MUTATION: (state: stateTypes, taskData: {id: string, text: string, updateMode?:boolean})  => {
    let base: Post;
    if(taskData.updateMode){
      base = state.updatePost;
    }else{
      base = state.newPost;
    }
    base = {...base, tasks: [...base.tasks.map(task => {
        if(task.id === taskData.id){
          return {
            ...task, mode: 'read'
          }
        }else{
          return task
        }
      })]}

    if(taskData.updateMode){
      state.updatePost = base;
    }else{
      state.newPost = base;
    }
  }
}
