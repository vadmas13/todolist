import Vue from "vue";
import Router from "vue-router";

import Index from "../pages/Index.vue";
import Post from "../pages/Post.vue";


Vue.use(Router);

export default new Router({
  mode: 'history',  // build - change to hash
  base: process.env.BASE_URL,
  routes: [
    { name: 'index', path: '/', component: Index },
    { name: 'post' , path: '/post/:id', component: Post },
    {
      path: '*',
      redirect: '/'
    }
  ]
})


